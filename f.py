from tools.finite_state_machine import FiniteStateMachine, State, ChangeTransitionBuilder

fsm = FiniteStateMachine(('a',))

s0 = State('0', True, True)
s1 = State('1', False, False)
s2 = State('2', True, False)
s3 = State('3', False, False)

fsm.states = [s0, s1, s2, s3]
fsm.add_transition(s0, s3, 'a')
fsm.add_transition(s2, s1, 'a')
fsm.add_transition(s1, s3, 'a')
fsm.add_transition(s3, s1, 'a')

fsm.check_if_the_word_is_acceptable()