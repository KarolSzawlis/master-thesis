from tools.finite_state_machine import State, StateException, InvalidTransitionException
import pytest
import copy


class TestFSM:

    def test_process_the_word(self, simple_fsm, initial_state):
        '''Process the word through the Finite State Machine'''
        simple_fsm.check_if_the_word_is_acceptable('aaaaba', initial_state)

    def test_incorrect_transition(self, simple_fsm, simple_state):
        '''Attempt to add a state that is not part of the Finite State Machine'''
        with pytest.raises(StateException):
            s = simple_fsm.get_state_by_name('1')
            s2 = State('outside state')
            simple_fsm.add_transition(s, s2, 'b')

    def test_state_to_state(self, simple_fsm):
        '''Add transition from state to state and then process the word'''
        new_initial_state = State('5', True, True)
        simple_fsm.add_state(new_initial_state)
        simple_fsm.add_transition(new_initial_state, new_initial_state, 'a')
        s2 = simple_fsm.get_state_by_name('1')
        simple_fsm.add_transition(new_initial_state, s2, 'b')
        assert simple_fsm.check_if_the_word_is_acceptable('ab', new_initial_state) == False

    def test_delete_transition(self, simple_fsm):
        '''Copy simple fstm then delete transition and process incorrect word'''
        simple_fstm2 = copy.copy(simple_fsm)
        s1 = simple_fstm2.get_state_by_name('1')
        s2 = simple_fstm2.get_state_by_name('2')
        assert simple_fstm2.check_if_the_word_is_acceptable('a',s2) == False
        simple_fstm2.delete_transition(s2, s1, 'a')
        with pytest.raises(InvalidTransitionException):
            simple_fstm2.check_if_the_word_is_acceptable('a', s2)