import pytest
from tools.finite_state_machine import State, FiniteStateMachine


@pytest.fixture
def simple_state() -> State:
    return State('1', False, False)


@pytest.fixture
def initial_state() -> State:
    return State('2', True, False)


@pytest.fixture
def end_state() -> State:
    return State('3', False, True)


@pytest.fixture
def combo_state() -> State:
    return State('4', True, True)


@pytest.fixture
def simple_fsm(initial_state, simple_state, combo_state, end_state):
    fsm = FiniteStateMachine(('a','b'))
    fsm.add_state(initial_state).add_state(simple_state).add_state(combo_state).add_state(end_state)
    fsm.add_transition(initial_state, simple_state, 'a')
    fsm.add_transition(simple_state, initial_state, 'b')
    fsm.add_transition(simple_state, combo_state, 'a')
    fsm.add_transition(combo_state, end_state, 'a')
    fsm.add_transition(end_state, initial_state, 'b')
    fsm.add_transition(end_state, simple_state, 'a')
    return fsm


