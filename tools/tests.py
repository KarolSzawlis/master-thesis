from tools.finite_state_machine import FiniteStateMachine


class Test:
    def __init__(self, word: str, initial_state: str):
        self.word = word
        self.initial_state = initial_state
        self.detected = set()
        self.loss_function = 0

    def calculate_loss_function(self):
        self.loss_function = len(self.detected) / len(self.word)
        return self

    def __str__(self):
        return str({
            "word": self.word,
            "detected": self.detected,
            "initial_state": self.initial_state,
            "loss_function": self.loss_function
        })


class TestsSet:
    def __init__(self, tests: [Test]):
        self.tests = tests
        self.loss_function = 0
        self.detected = set()

    def clear(self):
        self.loss_function = 0
        self.detected = set()
        for test in self.tests:
            test.loss_function = 0
            test.detected = set()
        return self

    def __recalculate(self):
        for test in self.tests:
            test.calculate_loss_function()

        self.tests.sort(key=lambda test: test.loss_function, reverse=True)
        tests = [test for test in self.tests if len(test.detected) > 0]
        self.tests = tests
        new_det = set()

        for test in self.tests:
            det = test.detected - new_det
            test.detected = det
            for d in test.detected:
                new_det.add(d)

        tests = [test for test in self.tests if len(test.detected) > 0]
        self.tests = tests
        self.tests.sort(key=lambda test: test.loss_function, reverse=True)

        for test in self.tests:
            test.calculate_loss_function()

        if sum(len(test.word) for test in self.tests) > 0:
            self.loss_function = sum(len(test.detected) for test in self.tests) / sum(
                len(test.word) for test in self.tests)
        else:
            self.loss_function = -1

        # detected = set()
        # for test in self.tests:
        #    test_detected = set()
        #    for d in test.detected:
        #        detected.add(d)
        #        test_detected.add(d)
        #    test.detected = test_detected
        # self.detected = detected
        # self.tests.sort(key=lambda test: test.loss_function, reverse=True)
        # added = self.detected
        # for test in self.tests:
        #    test.detected = test.detected.intersection(added)
        #    added = added - test.detected

        # tests = [test for test in self.tests if len(test.detected)]
        # self.tests = tests

        # for test in self.tests:
        #    test.calculate_loss_function()

    def detect_defects(self, invalid_machines: [FiniteStateMachine], machine: FiniteStateMachine):
        for test in self.tests:
            for invalid_machine in invalid_machines:
                invalid_machine_result = invalid_machine['machine'].check_if_the_word_is_acceptable(test.word,
                                                                                                    test.initial_state)
                original_machine_result = machine.check_if_the_word_is_acceptable(test.word, test.initial_state)
                if invalid_machine_result != original_machine_result:
                    self.detected.add(invalid_machine['index'])
                    test.detected.add(invalid_machine['index'])
            test.calculate_loss_function()
        if sum(len(test.word) for test in self.tests) > 0:
            self.loss_function = sum(len(test.detected) for test in self.tests) / sum(
                len(test.word) for test in self.tests)
        else:
            self.loss_function = -1

        self.__recalculate()
