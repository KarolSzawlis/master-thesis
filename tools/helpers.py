class NestedDict(dict):
    def __missing__(self, key):
        self[key] = NestedDict()
        return self[key]


def list_to_string(list: list):
    s = ""
    for l in list:
        s += l
    return s
