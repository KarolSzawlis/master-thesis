from copy import copy


class InvalidTransitionException(Exception):
    pass


class StateException(Exception):
    pass


class AlphabetException(Exception):
    pass


class State:
    def __init__(self, name: str, initial_state=False, end_state=False):
        self.name = name
        self.initial_state = initial_state
        self.end_state = end_state
        self.transitions = {}

    def _add_transition(self, state_to, letter: str):
        if letter not in self.transitions.keys():
            self.transitions[letter] = state_to
        else:
            raise InvalidTransitionException(
                f'There is already transition from state: {self.name} with letter: {letter}')

    def _delete_transition(self, state_to, letter: str):
        if letter not in self.transitions.keys():
            raise InvalidTransitionException(
                f'There is no transition with letter {letter}')
        elif self.transitions[letter] != state_to:
            raise InvalidTransitionException(
                f'There is no transition from {self.name} to {state_to.name} with letter {letter}')
        del self.transitions[letter]

    def _transit(self, letter: str):
        if letter not in self.transitions.keys():
            raise InvalidTransitionException(f'There is no transition from state {self.name} with letter {letter}')
        return self.transitions[letter]


class FiniteStateMachine:
    def __init__(self, alphabet: tuple):
        self.states: [State] = []
        self.alphabet = alphabet

    def get_state_by_name(self, name) -> State:
        try:
            return next(s for s in self.states if s.name == name)
        except StopIteration:
            raise StateException(f'Can not find state with name: {name} in this Finite State Machine')

    def add_state(self, state: State):
        if state.name in [s.name for s in self.states]:
            raise StateException(f'{state.name} already exists in this Finite State Machine')
        self.states.append(state)
        return self

    def add_transition(self, state_from: State, state_to: State, letter: str):
        states_names = [state.name for state in self.states]
        if state_from.name not in states_names:
            raise StateException(f'{state_from.name} is not a part of this Finite State Machine')
        elif state_to.name not in states_names:
            raise StateException(f'{state_to.name} is not a part of this Finite State Machine')
        state_from._add_transition(state_to, letter)

    def delete_transition(self, state_from, state_to, letter: str):
        if letter not in self.alphabet:
            raise AlphabetException(f'{letter} does not belongs to alphabet')
        states_names = [state.name for state in self.states]
        if state_from.name not in states_names:
            raise StateException(f'{state_from.name} is not a part of this Finite State Machine')
        elif state_to.name not in states_names:
            raise StateException(f'{state_to.name} is not a part of this Finite State Machine')
        state_from._delete_transition(state_to, letter)

    def check_if_the_word_is_acceptable(self, word: str, initial_state_name: str):
        initial_state = self.get_state_by_name(initial_state_name)
        if initial_state not in self.states:
            raise StateException(f'Can not find state with name: {initial_state.name} in this Finite State Machine')
        elif initial_state.initial_state is False:
            raise StateException(f'State {initial_state.name} is not not initial state!')
        actual_state = initial_state
        for w in word:
            if w not in self.alphabet:
                raise AlphabetException(f'{w} does not belongs to alphabet')
            actual_state = actual_state._transit(w)
        return actual_state.end_state

    def cohesion(self):
        visited = self.__dfs()
        not_visited = [state for state in self.states if state not in visited]

        for vis in self.states:
            transitions = {k: v for (k, v) in vis.transitions.items() if v  not in not_visited}
            vis.transitions = transitions

        self.states = visited

    def __dfs(self):
        initials = [state for state in self.states if state.initial_state is True]
        visited = []
        for initial in initials:
            current = initial
            vis = []
            queue = []
            queue.append(current)
            while len(queue) > 0:
                proc = queue.pop(0)
                vis.append(proc)
                for state in proc.transitions.values():
                    if state not in vis:
                        queue.append(state)

            visited += vis
        return list(set(visited))

    def __str__(self):
        rep = {}
        for s in self.states:
            trans = {}
            for t in s.transitions.keys():
                trans[t] = s.transitions[t].name
            rep[s.name] = trans
        rep['initial'] = [s.name for s in self.states if s.initial_state is True]
        rep['end'] = [s.name for s in self.states if s.end_state is True]
        return str(rep)

    def copy(self):
        fsm_copy = FiniteStateMachine(self.alphabet)
        states_copy = []
        for state in self.states:
            state_copy = State(state.name, state.initial_state, state.end_state)
            states_copy.append(state_copy)
        fsm_copy.states = states_copy
        for copy_state in fsm_copy.states:
            original_state = self.get_state_by_name(copy_state.name)
            for transition in original_state.transitions.keys():
                copy_to = fsm_copy.get_state_by_name(original_state.transitions[transition].name)
                fsm_copy.add_transition(copy_state, copy_to, transition)
        return fsm_copy


class ChangeTransitionBuilder:

    @classmethod
    def create_machines_with_defect(cls, fsm: FiniteStateMachine) -> [FiniteStateMachine]:
        # todo refactor to more clearer form
        machines_with_defect = []
        for state in fsm.states:
            for transition in state.transitions.keys():
                fsm_copy = fsm.copy()

                from_copy = fsm_copy.get_state_by_name(state.name)
                to_original = state.transitions[transition]
                to_copy = fsm_copy.get_state_by_name(to_original.name)
                fsm_copy.delete_transition(from_copy, to_copy, transition)

                for begin_state in fsm_copy.states:
                    for end_state in fsm_copy.states:
                        if begin_state.name == from_copy.name and end_state.name == to_copy.name:
                            continue
                        else:
                            if transition in begin_state.transitions.keys():
                                continue
                            else:
                                fsm_copy_2 = fsm_copy.copy()
                                fsm_copy_2.add_transition(fsm_copy_2.get_state_by_name(begin_state.name),
                                                          fsm_copy_2.get_state_by_name(end_state.name),
                                                          transition)
                                machines_with_defect.append(fsm_copy_2)

        return machines_with_defect


class DefectBuilder:
    @property
    def change_transition(self):
        return ChangeTransitionBuilder


