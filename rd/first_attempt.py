from tools.finite_state_machine import ChangeTransitionBuilder
from rd.generate_machines import machine_builder
from rd.generate_tests import build_tests
import math
import random
from tools.tests import TestsSet, Test


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[0:i + n]


machines = [machine_builder() for _ in range(10)]
res = []
resa = []

j = 0
for machine in machines:
    j += 1
    machine.cohesion()
    invalid_machines = ChangeTransitionBuilder().create_machines_with_defect(machine)
    invalid_machines = [{'machine': machine, 'index': index} for index, machine in enumerate(invalid_machines)]
    current_test_sets = [build_tests(machine) for _ in range(4 * len(invalid_machines))]
    initials = [state.initial_state for state in machine.states]
    end = [state.end_state for state in machine.states]
    if len(set(initials)) != 2 or len(set(end)) != 2:
        continue
    #rint(
    #    '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    #print(machine)
    tap = False
    for state in machine.states:
        for s in state.transitions.keys():
            if state.transitions[s].initial_state is True:
                tap = True
    if tap == False:
        continue

    for _ in range(100):
        for test_set in current_test_sets:
            test_set.detect_defects(invalid_machines, machine)

        current_test_sets = [test_set for test_set in current_test_sets if test_set.loss_function > 0]
        if _ == 0:
            current_test_sets.sort(key=lambda test_set: (len(test_set.detected), test_set.loss_function), reverse=True)
        test_chunks = list(chunks(current_test_sets, int(len(current_test_sets) / 4)))

        new_epoch = [current_test_sets[0]]

        for test_chunk in test_chunks:
            for _ in range(len(invalid_machines)):
                first_test_set_to_merge = random.choice(test_chunk)
                second_test_set_to_merge = random.choice(test_chunk)
                merger_first = []
                for test in first_test_set_to_merge.tests:
                    n_test = Test(test.word, test.initial_state)
                    n_test.detected = test.detected
                    n_test.loss_function = test.loss_function
                    merger_first.append(n_test)

                merger_second = []
                for test in second_test_set_to_merge.tests:
                    n_test = Test(test.word, test.initial_state)
                    n_test.detected = test.detected
                    n_test.loss_function = test.loss_function
                    merger_first.append(n_test)

                new_test_set = TestsSet(merger_first + merger_second)
                new_test_set.tests.sort(key=lambda test: (len(test.detected), test.loss_function), reverse=True)
                new_test_set.clear()
                new_test_set.detect_defects(invalid_machines, machine)
                tests = [t for t in new_test_set.tests if len(t.detected) > 0 or t.loss_function > 0]
                new_test_set.tests = tests
                new_epoch.append(new_test_set)
        current_test_sets = new_epoch
        current_test_sets.sort(key=lambda test_set: (len(test_set.detected), test_set.loss_function), reverse=True)

        # print('----------')
        # print(current_test_sets[0].detected)
        # print(current_test_sets[0].loss_function)
        # for test in current_test_sets[0].tests:
        #    print(test)
        # print('----------')
    print('???????????????????????????????????????')
    print(f'loss function: {current_test_sets[0].loss_function}')
    print(f'percentage: {len(current_test_sets[0].detected) / len(invalid_machines)}')

    if len(current_test_sets[0].detected) / len(invalid_machines) < 1:
        print(machine)
        print(len(current_test_sets[0].detected) / len(invalid_machines))
    print('???????????????????????????????????????')
    res.append(len(current_test_sets[0].detected) / len(invalid_machines))
    resa.append(current_test_sets[0].loss_function)

print(sum(resa) / len(resa))
print(sum(res) / len(res))
