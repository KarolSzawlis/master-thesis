from tools.finite_state_machine import FiniteStateMachine
from tools.tests import TestsSet, Test
import random
import numpy as np

random.seed(1)
np.random.seed(1)


def build_tests(fsm: FiniteStateMachine) -> TestsSet:
    amount_of_tests = random.randrange(1, 30)
    tests = []
    for _ in range(amount_of_tests):
        amount_of_words = random.randrange(1, 30)
        initials = [state for state in fsm.states if state.initial_state]
        first_state = random.choice(initials)
        route = ""
        state = first_state
        count = 0
        while count < amount_of_words:
            if not state.transitions:
                tests.append(Test(route, first_state))
                break
            new_letter = random.choice(list(state.transitions.keys()))
            route += str(new_letter)
            state = state.transitions[new_letter]
            count += 1
        tests.append(Test(route, first_state.name))
    return TestsSet(tests)
