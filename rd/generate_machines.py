from tools.finite_state_machine import State, InvalidTransitionException
from tools.tests import Test, TestsSet
import random
from tools.finite_state_machine import FiniteStateMachine, State, DefectBuilder, ChangeTransitionBuilder
import numpy as np
import string

'''
First experiment
'''

import random

random.seed(1)
np.random.seed(1)


def machine_builder() -> FiniteStateMachine:
    amount_of_states = random.randrange(3, 6)
    states = []
    s0 = State('0', True, True)
    states.append(s0)
    for i in range(1, amount_of_states):
        begin = random.choices([True, False], weights=[0.5, 0.5])[0]
        end = random.choices([True, False], weights=[0.5, 0.5])[0]
        s = State(str(i), begin, end)
        states.append(s)
    alph = list(string.ascii_lowercase)
    amount_of_transition = random.randrange(2, 4)
    alphabet = tuple(alph[:amount_of_transition - 1])
    fsm = FiniteStateMachine(alphabet)
    fsm.states = states
    amount_of_real_transition = 20000
    for _ in range(amount_of_real_transition):
        try:
            fsm.add_transition(random.choice(fsm.states), random.choice(fsm.states), random.choice(fsm.alphabet))
        except InvalidTransitionException:
            continue

    return fsm
